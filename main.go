package main

import (
	"encoding/json"
	"fmt"
	"os"

	"gitlab.com/lamb100/aes-string/aesstring8"
)

func main() {
	sources := os.Args
	for i := 1; i < len(sources); i++ {
		source := sources[i]
		fmt.Printf("[%s]:\n", source)
		encrypt, err := aesstring8.New(source)
		if nil != err {
			fmt.Printf("\tFail; error: %v \n", err)
			return
		}
		fmt.Printf("\t Encrypted: %s \n", encrypt.String())
		test, err := aesstring8.Parse(encrypt.String())
		if nil != err {
			fmt.Printf("\tFail; error: %v \n", err)
			return
		}
		fmt.Printf("\t Encrypted: %s \n", test)
		rev, err := encrypt.Get()
		if nil != err {
			fmt.Printf("\tFail; error: %v \n", err)
			return
		}
		fmt.Printf("\t Reversed: %s \n", rev)
		js, err := json.Marshal(encrypt)
		if nil != err {
			fmt.Printf("\tFail; error: %v \n", err)
			return
		}
		fmt.Printf("\t JSON: %s \n", js)
		var rjs aesstring8.AESString
		err = json.Unmarshal(js, &rjs)
		if nil != err {
			fmt.Printf("\tFail; error: %v \n", err)
			return
		}
		fmt.Printf("\t REV. JSON: %s \n", rjs)
	}
}
