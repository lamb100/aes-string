package aesstring64

import (
	"crypto/aes"
	"crypto/cipher"
	"crypto/sha512"
	"encoding/base64"
	"encoding/json"
	"encoding/xml"
	"fmt"
	"math/rand"

	"gitlab.com/lamb100/aes-string/aesstring"
)

// private functions
func getKeyIV(seed []byte) (key []byte, iv []byte) {
	temp := sha512.Sum512(seed)
	key = temp[0:32]
	iv = temp[32:48]
	return
}

func randSeed(l uint8) []byte {
	if l == 0 {
		return []byte{byte(rand.Intn(255))}
	}
	ret := []byte{byte(rand.Intn(255))}
	for i := uint8(0); i < l; i++ {
		ret = append(ret, byte(rand.Intn(255)))
	}
	return ret
}

func genKeyIV(keyLen uint8) (seed []byte, key []byte, iv []byte) {
	seed = randSeed(keyLen)
	key, iv = getKeyIV(seed)
	return
}

// public functions
func EncryptString(source string) ([]byte, error) {
	return Encrypt([]byte(source))
}

func Encrypt(source []byte) (ret []byte, err error) {
	ret = []byte{}
	seedLen := 64
	seed, key, iv := genKeyIV(uint8(seedLen))
	ret = append(ret, seed...)
	cif, err := aes.NewCipher(key)
	if err != nil {
		return
	}
	cfb := cipher.NewCFBEncrypter(cif, iv)
	bSource := []byte(source)
	bTarget := make([]byte, len(source))
	cfb.XORKeyStream(bTarget, bSource)
	ret = append(ret, bTarget...)
	return
}

func Decrypt(source []byte) (ret []byte, err error) {
	seedLen := 64
	seed := source[0 : int(seedLen)+1]
	key, iv := getKeyIV(seed)
	cif, err := aes.NewCipher(key)
	if err != nil {
		return
	}
	cfb := cipher.NewCFBDecrypter(cif, iv)
	lenSource := len(source)
	bSource := []byte(source[seedLen+1 : lenSource])
	ret = make([]byte, len(source))
	cfb.XORKeyStream(ret, bSource)
	return
}

func DecryptString(source []byte) (ret string, err error) {
	bTar, err := Decrypt(source)
	if err != nil {
		return
	}
	ret = string(bTar)
	return
}

// AESString Constructor
func New(source string) (AESString, error) {
	ret, err := EncryptString(source)
	if err != nil {
		return nil, err
	}
	return AESString(ret), nil
}

func Parse(base64String string) (AESString, error) {
	var (
		bRet []byte
		err  error
	)
	bRet, err = base64.StdEncoding.DecodeString(base64String)
	if err == nil {
		return AESString(bRet), nil
	}
	bRet, err = base64.RawStdEncoding.DecodeString(base64String)
	if err == nil {
		return AESString(bRet), nil
	}
	bRet, err = base64.RawStdEncoding.DecodeString(base64String)
	if err == nil {
		return AESString(bRet), nil
	}
	bRet, err = base64.URLEncoding.DecodeString(base64String)
	if err == nil {
		return AESString(bRet), nil
	}
	return nil, fmt.Errorf("bad string,error: %v", err)
}

func (o AESString) Get() (ret string, err error) {
	return DecryptString([]byte(o))
}

func (o AESString) Match(str string) (m bool, err error) {
	t, err := o.Get()
	if err != nil {
		return
	}
	m = t == str
	return
}

func (o AESString) String(opt ...aesstring.Base64Option) string {
	var opti aesstring.Base64Option
	if len(opt) <= 0 {
		opti = 0
	} else {
		opti = opt[0]
	}
	switch opti {
	case aesstring.Base64Filled:
		return base64.StdEncoding.EncodeToString([]byte(o))
	case aesstring.Base64URL:
		return base64.URLEncoding.EncodeToString([]byte(o))
	case (aesstring.Base64Filled | aesstring.Base64URL):
		return base64.RawURLEncoding.EncodeToString([]byte(o))
	default:
		return base64.RawStdEncoding.EncodeToString([]byte(o))
	}
}

func (o AESString) MarshalJSON() ([]byte, error) {
	ret := o.String()
	return json.Marshal(ret)
}

func (o *AESString) UnmarshalJSON(data []byte) error {
	var (
		sRet string
		m    AESString
	)
	err := json.Unmarshal(data, &sRet)
	if err != nil {
		return err
	}
	m, err = Parse(sRet)
	if err != nil {
		return err
	}
	*o = m
	return nil
}

func (o *AESString) UnmarshalXML(d *xml.Decoder, start xml.StartElement) error {
	var s string
	if err := d.DecodeElement(&s, &start); err != nil {
		return err
	}
	a, err := Parse(s)
	if err != nil {
		return err
	}
	*o = a
	return nil
}
func (o AESString) MarshalXML(e *xml.Encoder, start xml.StartElement) error {
	var s string
	s = o.String()
	return e.EncodeElement(s, start)
}
func (o *AESString) UnmarshalText(text []byte) error {
	a, err := Parse(string(text))
	if err != nil {
		return err
	}
	*o = a
	return nil
}
func (o AESString) MarshalText() ([]byte, error) {
	s := o.String()
	return []byte(s), nil
}
