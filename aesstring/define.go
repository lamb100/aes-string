package aesstring

type AESString []byte

type Base64Option uint8

const(
	Base64Filled Base64Option = 1
	Base64URL Base64Option = 2
)